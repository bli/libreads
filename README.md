# Utilities to generate stats and graphs from reads.

This is a very preliminary version, with very few utilities.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libreads.git`, `cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libreads.git

