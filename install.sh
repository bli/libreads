#!/bin/sh
/usr/bin/env python3 setup.py build_ext
# .egg-link does not work with PYTHONPATH ?
# https://github.com/pypa/setuptools/issues/1405#issuecomment-409663455
/usr/bin/env python3 -m pip install --no-build-isolation -e .
/usr/bin/env python3 -m pip install --no-build-isolation --no-deps --ignore-installed .
