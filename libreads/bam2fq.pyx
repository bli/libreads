#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# cython: language_level=3
"""
This library contains a cythonized version of utilities to work with bam files.
"""

# http://stackoverflow.com/a/23575771/1878788
# Also works by setting language_level as comment
# at the top of the file.
#from __future__ import print_function
#from pybedtools import Interval
from pysam.libchtslib cimport BAM_FREVERSE
#cdef int BAM_FREVERSE = 16
#from pysam.libcalignmmentfile cimport AlignmentFile
from pysam.libcalignedsegment cimport AlignedSegment
from pysam.libcutils cimport array_to_qualitystring


cdef str cali2fq(AlignedSegment ali):
    """Return a string representing the read aligned in AlignedSegment *ali*"""
    seq = ali.get_forward_sequence()
    qual = array_to_qualitystring(ali.get_forward_qualities())
    name = ali.query_name
    return f"@{name}\n{seq}\n+\n{qual}\n"


def ali2fq(ali):
    return cali2fq(ali)


def bam2fastq(bam_reads,
             chrom,
             start,
             end,
             strand=None,
             min_read_len=None,
             max_read_len=None):
    """Generate fastq representations of reads mapped in *bam_reads*.
    Only reads in region defined by *chrom*, *start* and *end* are considered.
    If *strand* is "-" or "+", only the reads from the corresponding strand
    will be generated.
    If *min_read_len* is set, only the reads with a length
    at least *min_read_len* will be generated.
    If *max_read_len* is set, only the reads with a length
    at most *max_read_len* will be generated.
    """
    if strand is None:
        if min_read_len is None and max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                yield cali2fq(ali)
        elif min_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.query_length <= max_read_len:
                    yield cali2fq(ali)
        elif max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if min_read_len <= ali.query_length:
                    yield cali2fq(ali)
        else:
            for ali in bam_reads.fetch(chrom, start, end):
                if min_read_len <= ali.query_length <= max_read_len:
                    yield cali2fq(ali)
    elif strand == "+":
        if min_read_len is None and max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE == 0:
                    yield cali2fq(ali)
        elif min_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE == 0:
                    if ali.query_length <= max_read_len:
                        yield cali2fq(ali)
        elif max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE == 0:
                    if min_read_len <= ali.query_length:
                        yield cali2fq(ali)
        else:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE == 0:
                    if min_read_len <= ali.query_length <= max_read_len:
                        yield cali2fq(ali)
    elif strand == "-":
        if min_read_len is None and max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE != 0:
                    yield cali2fq(ali)
        elif min_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE != 0:
                    if ali.query_length <= max_read_len:
                        yield cali2fq(ali)
        elif max_read_len is None:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE != 0:
                    if min_read_len <= ali.query_length:
                        yield cali2fq(ali)
        else:
            for ali in bam_reads.fetch(chrom, start, end):
                if ali.flag & BAM_FREVERSE != 0:
                    if min_read_len <= ali.query_length <= max_read_len:
                        yield cali2fq(ali)
    else:
        raise ValueError("strand can only be \"+\", \"-\" or None.")


# def bam2signal(bam_reads,
#                chrom: str,
#                start: int,
#                end: int,
#                max_read_len: int) -> Mapping[str, Array[int]]:
#     """
#     Extract 5'-end signal from `pysam.AlignmentFile` *bam_reeads*
#     for the region defined by *chrom*, *start* and *end*.
#     """
#     signal: Mapping[str, Array[int]] = defaultdict(
#         lambda: np.zeros(end-start + max_read_len, dtype=int))
#     for read in collapse_and_sort(bam_reads.fetch(chrom, start, end)):
#         try:
#             # subttracting start, to translate chromosomic coordinates into array indices
#             signal[read.strand][read.start - start] += 1
#         except IndexError as err:
#             # 5' of reverse reads may fall outside the right-most boundary of the island
#             print(str(err))
#             print(len(signal[read.strand]))
#             print(read)
#     return signal



def main():
    """Example use case."""
    print("No examples to show here.")
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main())
