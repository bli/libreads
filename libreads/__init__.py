__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
__version__ = 0.9
from .bam2fq import ali2fq, bam2fastq
from .libreads import (
    READ_STAT_NAMES,
    ReadStats,
    make_read_statter,
    make_read_stats,
    plot_base_logos_along_read,
    plot_base_proportions_along_read,
    plot_base_scaled_proportions_along_read,
    plot_base_logos_by_size,
    )
