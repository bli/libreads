# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Module providing utilities to generate graphs and stats about reads."""

# import warnings
# from collections import defaultdict
from functools import reduce
from itertools import islice
from concurrent.futures import ProcessPoolExecutor
# from operator import or_ as union
from operator import attrgetter
# from cytoolz import compose
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mappy import fastx_read


# def formatwarning(message, category, filename, lineno, line):
#     """Used to format warning messages."""
#     return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


# warnings.formatwarning = formatwarning
def make_read_statter(min_len=None, max_len=None, alphabet=None, processes=1):
    """
    Create a function that generates read stats from a fasta/fastq file.

    A minimum (*min_len*, default 1) and maximum (*max_len*, default 50)
    read length are used to set the size of the "composition_from_start"
    and "composition_from_end" output tables.
    """
    if min_len is None:
        min_len = 1
    if max_len is None:
        max_len = 50
    assert min_len <= max_len
    size_range = range(min_len, max_len + 1)
    if alphabet is None:
        alphabet = "ACGTN"
    slicer_from_start = slice(None)
    slicer_from_end = slice(max_len - 1, None, -1)
    # To get the row corresponding to a given nucleotide in a composition table
    letter2index = dict((letter, idx) for (idx, letter) in enumerate(alphabet))
    # To use as DataFrame index
    base_index = pd.Index(list(alphabet), name="base")
    # TODO: try to speed this using ProcessPoolExecutor?

    def extract_seq_info(seq):
        seq_len = len(seq)
        # if seq_len > max_len:
        #     # raise ValueError(
        #     #     f"{fastx_filename} contains reads of size {seq_len} "
        #     #     f"(> {max_len})")
        #     raise ValueError(
        #         f"The file contains reads of size {seq_len} ({seq})"
        #         f"(> {max_len})")
        if seq_len == 0:
            raise ValueError(
                "The file contains reads of size zero")
        # Using -1 for unknown letters
        # This will have to be correctly filtered out later,
        # because negative indices are valid.
        as_letter_indices = [letter2index.get(letter, -1) for letter in seq]
        letter_indices_from_start = as_letter_indices[slicer_from_start]
        letter_indices_from_end = as_letter_indices[slicer_from_end]
        return (seq_len, seq[0], seq[-1],
                letter_indices_from_start,
                letter_indices_from_end,
                range(seq_len))

    if processes > 1:
        executor = ProcessPoolExecutor(max_workers=processes)

        def do_extract_seq_info(seq_generator):
            yield from executor.map(
                extract_seq_info, seq_generator, chunksize=10000)
    else:
        def do_extract_seq_info(seq_generator):
            yield from map(extract_seq_info, seq_generator)

    def fastx2read_stats(fastx_filename):
        """Return statistics about reads from file *fastx_filename*.

        The statistics consist in a dict of tables with the following keys:
        * "total": total number of reads as a :class:`pandas.Series`
        * "size": read size distribution as a :class:`pandas.Series`
        * "composition_at_start"
        * "composition_at_end"
        * "composition_from_start"
        * "composition_from_end"
        * "proportions_along_from_start"
        * "proportions_along_from_end"

        Each of the composition and proportions tables is a
        :class:`pandas.DataFrame` with *max_len* columns.

        "composition" tables contain counts.
        "proportion" tables are derived from "composition" tables.
        """
        size = {i: 0 for i in size_range}
        first_base = {
            i: {letter: 0 for letter in alphabet} for i in size_range}
        last_base = {
            i: {letter: 0 for letter in alphabet} for i in size_range}
        composition_from_start = np.zeros((len(alphabet), max_len))
        composition_from_end = np.zeros((len(alphabet), max_len))

        seq_generator = (seq for (_, seq, *_) in fastx_read(fastx_filename))
        for (seq_len, start_letter, end_letter,
             letter_indices_from_start,
             letter_indices_from_end,
             positions) in do_extract_seq_info(seq_generator):
            if seq_len < min_len or seq_len > max_len:
                continue
            # (0, 0) <= (-1, 0) == False
            # (0, 0).__le__((-1, 0)) == False
            # -> we keep only (letter_idx, pos_in_seq) coordinates
            # where letter_idx is >= 0
            # Then we reconstitute a pair
            # of lists of letter indices and positions
            # to specify which letter was found where for this sequence.
            composition_from_start[
                tuple(zip(*filter(
                    (0, 0).__le__,
                    zip(letter_indices_from_start, positions))))] += 1
            composition_from_end[
                tuple(zip(*filter(
                    (0, 0).__le__,
                    zip(letter_indices_from_end, positions))))] += 1
            # composition_from_start[letter_indices_from_start, positions] += 1
            # composition_from_end[letter_indices_from_end, positions] += 1
            size[seq_len] += 1
            if start_letter in alphabet:
                first_base[seq_len][start_letter] += 1
            if end_letter in alphabet:
                last_base[seq_len][end_letter] += 1
        # Finalize as DataFrame
        composition_at_start = pd.DataFrame.from_dict(first_base).reindex(
            base_index)
        composition_at_start.columns.name = "read length"
        composition_at_start = composition_at_start.fillna(0)
        composition_at_end = pd.DataFrame.from_dict(last_base).reindex(
            base_index)
        composition_at_end.columns.name = "read length"
        composition_at_end = composition_at_end.fillna(0)
        composition_from_start = pd.DataFrame(
            composition_from_start, index=base_index)
        composition_from_start.columns = pd.Index(
            range(1, max_len + 1), name="position_from_start")
        composition_from_start = composition_from_start.fillna(0)
        composition_from_end = pd.DataFrame(
            composition_from_end, index=base_index)
        composition_from_end.columns = pd.Index(
            range(1, max_len + 1), name="position_from_end")
        composition_from_end = composition_from_end.fillna(0)
        # compute proportions, replacing division by zero NaNs by 0
        proportions_along_from_start = composition_from_start.div(
            composition_from_start.sum()).fillna(0)
        proportions_along_from_end = composition_from_end.div(
            composition_from_end.sum()).fillna(0)
        return {
            "total": pd.Series({"total": sum(size.values())}),
            "size": pd.Series(size).sort_index(),
            "composition_at_start": composition_at_start,
            "composition_at_end": composition_at_end,
            "composition_from_start": composition_from_start,
            "composition_from_end": composition_from_end,
            "proportions_along_from_start": proportions_along_from_start,
            "proportions_along_from_end": proportions_along_from_end,
            }

    return fastx2read_stats


READ_STAT_NAMES = [
    "total", "size",
    "composition_at_start", "composition_at_end",
    "composition_from_start", "composition_from_end"]


def make_read_stats(fastx_filename,
                    out_filenames=None,
                    min_len=None, max_len=None,
                    alphabet=None, threads=1):
    """
    Make various read statistics for reads in file *fastx_filename*.
    The statistics are saved in .tsv format in the files given by
    the dict *out_filenames*, where keys are statistics types,
    and values are paths to a file to be written.
    """
    fastx2read_stats = make_read_statter(
        min_len=min_len, max_len=max_len,
        alphabet=alphabet, processes=threads)

    read_stats = fastx2read_stats(str(fastx_filename))

    if out_filenames is not None:
        for (stat_type, stat_table) in read_stats.items():
            stat_table.to_csv(
                out_filenames[stat_type],
                sep="\t", header=False)
    return read_stats


def average_tables(tables):
    """
    Compute the cell-wise mean of the list of
    :class:`pandas.dataframe`s *tables*."""
    return reduce(pd.DataFrame.add, tables).div(len(tables))


def average_tables_np(tables):
    """Six times faster version proposed by Bertrand Neron."""
    return pd.DataFrame(
        np.array([np.array(table) for table in tables]).mean(axis=0),
        index=tables[0].index)


def try_average_tables(tables):
    """
    Compute the cell-wise mean of the list of
    :class:`pandas.dataframe`s *tables*.
    In case of failure, None is returned.
    """
    try:
        return reduce(pd.DataFrame.add, tables).div(len(tables))
    except ValueError:
        return None


def try_average_tables_np(tables):
    """Using the six times faster version proposed by Bertrand Neron."""
    try:
        return pd.DataFrame(
            np.array([np.array(table) for table in tables]).mean(axis=0),
            index=tables[0].index)
    except ValueError:
        return None


class ReadStats:
    """Object holding statistics about a set of reads as attributes."""
    __slots__ = (
        "alphabet", "min_len", "max_len", "total", "size",
        "composition_at_start", "composition_at_end",
        "composition_from_start", "composition_from_end",
        "proportions_along_from_start", "proportions_along_from_end",
        "scaled_proportions_along_from_start",
        "scaled_proportions_along_from_end",
        )

    @staticmethod
    def fastx2stats_maker(alphabet=None, min_len=None, max_len=None):
        """
        Make a function generating read stats from a fastq or fasta file.

        Minimum and maximum read lengths as well as alphabet can be pre-set.
        """
        def fastx2stats(fastx_filename):
            """
            Generate a ReadStats object from reads founs in *fastx_filename*.
            """
            return ReadStats(
                fastx_filename=fastx_filename,
                alphabet=alphabet,
                min_len=min_len, max_len=max_len)
        return fastx2stats

    @staticmethod
    def merge_from_iterable(read_stats):
        """Merge the ReadStats objects in *read_stats*.

        The statistics are expected to consist in the following
        attributes:

        * "total": total number of reads as a :class:`pandas.Series`
        * "size": read size distribution as a :class:`pandas.Series`
        * "composition_at_start"
        * "composition_at_end"
        * "composition_from_start"
        * "composition_from_end"
        * "proportions_along_from_start"
        * "proportions_along_from_end"

        The function returns a similar structure, where a sum over the inputs
        is used to determine the resulting "total" and "size",
        and composition tables.
        A mean is used to determine the resulting proportion tables.

        If possible, "scaled_proportions_along_from_start"
        and "scaled_proportions_along_from_end"
        are also determined using a mean.
        """
        # Only allow same alphabet everywhere
        (alphabet,) = set(map(attrgetter("alphabet"), read_stats))
        return ReadStats(
            alphabet=alphabet,
            # alphabet="".join(reduce(
            #     union,
            #     map(
            #         compose(set, attrgetter("alphabet")),
            #         read_stats))),
            min_len=min(*map(attrgetter("min_len"), read_stats)),
            max_len=max(*map(attrgetter("max_len"), read_stats)),
            total=sum(map(attrgetter("total"), read_stats)),
            size=sum(map(attrgetter("size"), read_stats)),
            # These are counts -> merge with sums
            composition_at_start=sum(map(
                attrgetter("composition_at_start"),
                read_stats)),
            composition_at_end=sum(map(
                attrgetter("composition_at_end"),
                read_stats)),
            composition_from_start=sum(map(
                attrgetter("composition_from_start"),
                read_stats)),
            composition_from_end=sum(map(
                attrgetter("composition_from_end"),
                read_stats)),
            # Here averages seem OK
            proportions_along_from_start=average_tables_np(list(map(
                attrgetter("proportions_along_from_start"),
                read_stats))),
            proportions_along_from_end=average_tables_np(list(map(
                attrgetter("proportions_along_from_end"),
                read_stats))),
            scaled_proportions_along_from_start=try_average_tables_np(list(map(
                attrgetter("scaled_proportions_along_from_start"),
                read_stats))),
            scaled_proportions_along_from_end=try_average_tables_np(list(map(
                attrgetter("scaled_proportions_along_from_end"),
                read_stats))))

    def __init__(self, fastx_filename=None,
                 alphabet=None, min_len=None, max_len=None,
                 threads=1,
                 **read_stats):
        if alphabet is None:
            self.alphabet = "ACGT"
        else:
            self.alphabet = alphabet
        if min_len is None:
            self.min_len = 1
        else:
            self.min_len = min_len
        if max_len is None:
            self.max_len = 50
        else:
            self.max_len = max_len
        # Should be computed later, but pylint can't know.
        self.proportions_along_from_start = None
        self.proportions_along_from_end = None
        # Can be computed later, given some background composition.
        self.scaled_proportions_along_from_start = None
        self.scaled_proportions_along_from_end = None
        # Overrides argument *read_stats*
        if fastx_filename is not None:
            read_stats = make_read_stats(
                fastx_filename,
                out_filenames=None, alphabet=alphabet,
                min_len=min_len,
                max_len=max_len,
                threads=threads)
        for stat_type, stat_value in read_stats.items():
            setattr(self, stat_type, stat_value)

    def scale_proportions(self, background_table):
        """
        Use data from tab-separated file *background_table* to
        scale base proportions to pre-computed background proportions.

        If a base is not present in the background, NaNs resulting
        from division by zero are replaced by zero.
        This is quite wrong unless the values to be divided
        are close to zero.
        """
        background = pd.read_table(
            background_table, sep="\t", index_col=0)
        self.scaled_proportions_along_from_start = \
            self.proportions_along_from_start.div(
                background.iloc[:, 0], axis=0).fillna(0)
        self.scaled_proportions_along_from_end = \
            self.proportions_along_from_end.div(
                background.iloc[:, 0], axis=0).fillna(0)


#######################
# Plotting read stats #
#######################
LETTER2COLOUR = {
    "A": "crimson",
    "C": "mediumblue",
    "G": "yellow",
    "T": "forestgreen",
    "N": "black"}
# base_colours = [LETTER2COLOUR.get(letter, "black") for letter in "ACGTN"]


def vstack_fig_setup(datas, data_labels):
    """Code common to several figures where subplots are vertically stacked."""
    n_data = len(datas)
    (fig, axes) = plt.subplots(n_data, 1, sharex=True, figsize=(15, 3*n_data))
    if n_data == 1:
        axes = [axes]
    # https://stackoverflow.com/a/36542971/1878788
    # add a big axes, hide frame
    fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    plt.tick_params(
        labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.grid(False)
    if data_labels is None:
        data_labels = [f"{i+1}" for i in range(n_data)]
    if len(data_labels) < n_data:
        raise ValueError(
            f"Not enough labels in:\n"
            f"{data_labels}\n"
            f"There are {n_data} datasets.\n")
    if len(data_labels) > n_data:
        raise ValueError(
            f"Too many labels in:"
            f"\n{data_labels}\n"
            f"There are {n_data} datasets.\n")
    return (fig, axes, data_labels)


def plot_proportions(
        data, axis,
        flip_bars=False, xlabel=None, ylabel=None,
        legend=None, alphabet=None, letter2legend=None):
    """*data* should have nucleotides as rows."""
    if alphabet is None:
        alphabet = "ACGTN"
    if flip_bars:
        rel_heights_data = data.T.applymap((-1.).__mul__)
        axis.set_ylim([-1, 0])
        bar_kwds = {"edgecolor": "orange"}
    else:
        rel_heights_data = data.T
        axis.set_ylim([0, 1])
        bar_kwds = {"edgecolor": "violet"}
    axis.set_xlim([
        rel_heights_data.index[0] - 0.5,
        rel_heights_data.index[-1] + 0.5])
    if letter2legend is None:
        letter2legend = dict(zip(alphabet, alphabet))
    for (read_len, rel_heights) in rel_heights_data.iterrows():
        # Manually stack the bars, in order to have the largest at the top
        y_base = 0
        for (letter, rel_height) in rel_heights.sort_values(
                ascending=(not flip_bars)).iteritems():
            axis.bar(
                read_len,
                rel_height,
                bottom=y_base,
                align="center",
                # width=0.33,
                width=0.5,
                color=LETTER2COLOUR[letter],
                label=letter2legend[letter],
                **bar_kwds)
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            y_base += rel_height
    if legend is None or legend or legend == "":
        axis.legend()
    axis.set_xticks(rel_heights_data.index)
    axis.set_xticklabels(rel_heights_data.index)
    if flip_bars:
        axis.set_yticklabels(abs(axis.get_yticks()))
    if xlabel is None:
        axis.set_xlabel("read_length")
    elif xlabel or xlabel == "":
        axis.set_xlabel(xlabel)
    if ylabel is None:
        axis.set_ylabel("Proportions")
    elif ylabel or ylabel == "":
        axis.set_ylabel(ylabel)
    # return legend handles and labels
    return (axis.get_legend_handles_labels(), rel_heights_data)


def plot_scaled_proportions(
        data, axis,
        flip_bars=False, xlabel=None, ylabel=None,
        legend=None, alphabet=None, letter2legend=None):
    """*data* should have nucleotides as rows."""
    if alphabet is None:
        alphabet = "ACGTN"
    if flip_bars:
        rel_heights_data = data.T.applymap((-1.).__mul__)
        axis.set_ylim([
            np.nanmin(rel_heights_data.sum(axis=1)),
            0])
        bar_kwds = {"edgecolor": "orange"}
    else:
        rel_heights_data = data.T
        axis.set_ylim([
            0,
            np.nanmax(rel_heights_data.sum(axis=1))])
        bar_kwds = {"edgecolor": "violet"}
    # axis.set_yscale("symlog")
    axis.set_xlim([
        rel_heights_data.index[0] - 0.5,
        rel_heights_data.index[-1] + 0.5])
    if letter2legend is None:
        letter2legend = dict(zip(alphabet, alphabet))
    for (read_len, rel_heights) in rel_heights_data.iterrows():
        # Manually stack the bars, in order to have the largest at the top
        y_base = 0
        for (letter, rel_height) in rel_heights.sort_values(
                ascending=(not flip_bars)).iteritems():
            axis.bar(
                read_len,
                rel_height,
                bottom=y_base,
                align="center",
                # width=0.33,
                width=0.5,
                color=LETTER2COLOUR[letter],
                label=letter2legend[letter],
                **bar_kwds)
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            y_base += rel_height
    if legend is None or legend or legend == "":
        axis.legend()
    axis.set_xticks(rel_heights_data.index)
    axis.set_xticklabels(rel_heights_data.index)
    if flip_bars:
        axis.set_yticklabels(abs(axis.get_yticks()))
    if xlabel is None:
        axis.set_xlabel("read_length")
    elif xlabel or xlabel == "":
        axis.set_xlabel(xlabel)
    if ylabel is None:
        axis.set_ylabel("Proportions (relative to background)")
    elif ylabel or ylabel == "":
        axis.set_ylabel(ylabel)
    # return legend handles and labels
    return (axis.get_legend_handles_labels(), rel_heights_data)


def entropies(data, axis=0):
    """By default, *data* should have nucleotides as rows.
    Otherwise, set *axis=1*."""
    return -(data * np.log2(data)).sum(axis=axis)


def proportions2information(data, axis=0):
    """By default, *data* should have nucleotides as rows.
    Otherwise, set *axis=1*."""
    if axis == 1:
        information = np.log2(len(data.columns)) - entropies(data, axis=1)
    elif axis != 0:
        raise ValueError("axis can only be 0 or 1.")
    else:
        information = np.log2(len(data.index)) - entropies(data)
    return information


def plot_logo(data, axis,
              flip_bars=False, xlabel=None, ylabel=None,
              legend=None, alphabet=None, letter2legend=None):
    """*data* should have nucleotides as rows."""
    if alphabet is None:
        alphabet = "ACGTN"
    information = proportions2information(data)
    if flip_bars:
        rel_heights_data = data.mul(information).T.applymap((-1.).__mul__)
        axis.set_ylim([-2, 0])
        bar_kwds = {"edgecolor": "orange"}
    else:
        rel_heights_data = data.mul(information).T
        axis.set_ylim([0, 2])
        bar_kwds = {"edgecolor": "violet"}
    axis.set_xlim([
        rel_heights_data.index[0] - 0.5,
        rel_heights_data.index[-1] + 0.5])
    if letter2legend is None:
        letter2legend = dict(zip(alphabet, alphabet))
    for (read_len, rel_heights) in rel_heights_data.iterrows():
        # Manually stack the bars, in order to have the largest at the top
        y_base = 0
        for (letter, rel_height) in rel_heights.sort_values(
                ascending=(not flip_bars)).iteritems():
            axis.bar(
                read_len,
                rel_height,
                bottom=y_base,
                align="center",
                # width=0.33,
                width=0.5,
                color=LETTER2COLOUR[letter],
                label=letter2legend[letter],
                **bar_kwds)
            # Avoid redundant legends after this letter
            # has been labeled a first time
            letter2legend[letter] = "_nolegend_"
            y_base += rel_height
    if legend is None or legend or legend == "":
        axis.legend()
    axis.set_xticks(rel_heights_data.index)
    axis.set_xticklabels(rel_heights_data.index)
    if flip_bars:
        axis.set_yticklabels(abs(axis.get_yticks()))
    if xlabel is None:
        axis.set_xlabel("read_length")
    elif xlabel or xlabel == "":
        axis.set_xlabel(xlabel)
    if ylabel is None:
        axis.set_ylabel("Information (bits)")
    elif ylabel or ylabel == "":
        axis.set_ylabel(ylabel)
    # return legend handles and labels
    return (axis.get_legend_handles_labels(), rel_heights_data)


def plot_base_logos_by_size(datas,
                            data_labels=None, fig_path=None,
                            ylabel=None, alphabet=None, letter2legend=None,
                            fig=None, axes=None, flip_bars=False, ylim=None,
                            title=None):
    """
    Plot bar graphs representing read composition using a representation
    where bar heights are proportional to the information content.

    *datas* should be an iterable of tables where the rows correspond to the
    nucleotides and the columns correspond to read lengths. A cell is expected
    to contain the proportion of each nucleotide at a certain position of the
    reads for reads of the corresponding read length.

    If provided, *data_labels* will be used to label the individual bar plots,
    and should be an iterable of labels matching *datas*.

    If *fig_path* is set, the figure will be saved using this file path.
    By default, no figure is saved.
    """
    if alphabet is None:
        alphabet = "ACGTN"
    if data_labels is None:
        data_labels = [str(i) for i in range(len(datas))]
    if axes is None:
        (fig, axes, data_labels) = vstack_fig_setup(datas, data_labels)
        legend_holder = fig
    else:
        if fig is None:
            legend_holder = axes[0]
        else:
            legend_holder = fig
    plotted_data = {}
    for (nucl_table, axis, label) in zip(datas, axes, data_labels):
        ((handles, labels), used_data) = plot_logo(
            nucl_table, axis=axis, flip_bars=flip_bars,
            legend=False, alphabet=alphabet, letter2legend=letter2legend,
            xlabel=False, ylabel=False)
        plotted_data[label] = used_data
        if ylim is not None:
            axis.set_ylim(ylim)
            axis.set_yticklabels(abs(axis.get_yticks()))
        axis.set_ylabel(label)
        axis.set_xlabel("")
        axis.yaxis.set_label_position("right")
    plt.xlabel("Read length", labelpad=10)
    if ylabel is None:
        ylabel = "Information content (bits)"
    plt.ylabel(ylabel, labelpad=30)
    plt.title("")
    label2handle = dict(islice(zip(labels, handles), 5))
    legend_holder.legend(
        handles=[label2handle[nucl] for nucl in alphabet],
        labels=alphabet,
        loc=7)
    if title is not None:
        fig.suptitle(title)
    if fig_path is not None:
        plt.savefig(fig_path)
    return (fig, axes, data_labels, plotted_data)


def plot_base_logos_along_read(datas,
                               data_labels=None, fig_path=None,
                               xlabel=None, ylabel=None,
                               alphabet=None, letter2legend=None,
                               fig=None, axes=None, flip_bars=False, ylim=None,
                               title=None):
    """
    Plot bar graphs representing read composition using a representation
    where bar heights are proportional to the information content.

    *datas* should be an iterable of tables where the rows correspond to the
    nucleotides and the columns correspond to positions along a read. A cell is
    expected to contain the proportion of each nucleotide at the corresponding
    position along the reads.

    If provided, *data_labels* will be used to label the individual bar plots,
    and should be an iterable of labels matching *datas*.

    If *fig_path* is set, the figure will be saved using this file path.
    By default, no figure is saved.
    """
    if alphabet is None:
        alphabet = "ACGTN"
    if data_labels is None:
        data_labels = [str(i) for i in range(len(datas))]
    if axes is None:
        (fig, axes, data_labels) = vstack_fig_setup(datas, data_labels)
        legend_holder = fig
    else:
        if fig is None:
            legend_holder = axes[0]
        else:
            legend_holder = fig
    plotted_data = {}
    for (nucl_table, axis, label) in zip(datas, axes, data_labels):
        ((handles, labels), used_data) = plot_logo(
            nucl_table, axis=axis, flip_bars=flip_bars,
            legend=False, alphabet=alphabet, letter2legend=letter2legend,
            xlabel=False, ylabel=False)
        plotted_data[label] = used_data
        if ylim is not None:
            axis.set_ylim(ylim)
            axis.set_yticklabels(abs(axis.get_yticks()))
        axis.set_ylabel(label)
        axis.set_xlabel("")
        axis.yaxis.set_label_position("right")
    if xlabel is None:
        xlabel = "Position in read"
    plt.xlabel(xlabel, labelpad=10)
    if ylabel is None:
        ylabel = "Information content (bits)"
    plt.ylabel(ylabel, labelpad=30)
    plt.title("")
    label2handle = dict(islice(zip(labels, handles), 5))
    legend_holder.legend(
        handles=[label2handle[nucl] for nucl in alphabet],
        labels=alphabet,
        loc=7)
    if title is not None:
        fig.suptitle(title)
    if fig_path is not None:
        plt.savefig(fig_path)
    return (fig, axes, data_labels, plotted_data)


def plot_base_proportions_along_read(
        datas,
        data_labels=None, fig_path=None,
        xlabel=None, ylabel=None,
        alphabet=None, letter2legend=None,
        fig=None, axes=None, flip_bars=False, ylim=None,
        title=None):
    """
    Plot bar graphs representing read composition using a representation
    where bars are stacks where nucleotides are represented according
    to their proportion at a given position in reads.

    *datas* should be an iterable of tables where the rows correspond to the
    nucleotides and the columns correspond to positions along a read. A cell is
    expected to contain the proportion of each nucleotide at the corresponding
    position along the reads.

    If provided, *data_labels* will be used to label the individual bar plots,
    and should be an iterable of labels matching *datas*.

    If *fig_path* is set, the figure will be saved using this file path.
    By default, no figure is saved.
    """
    if alphabet is None:
        alphabet = "ACGTN"
    if data_labels is None:
        data_labels = [str(i) for i in range(len(datas))]
    if axes is None:
        (fig, axes, data_labels) = vstack_fig_setup(datas, data_labels)
        legend_holder = fig
    else:
        if fig is None:
            legend_holder = axes[0]
        else:
            legend_holder = fig
    plotted_data = {}
    for (nucl_table, axis, label) in zip(datas, axes, data_labels):
        ((handles, labels), used_data) = plot_proportions(
            nucl_table, axis=axis, flip_bars=flip_bars,
            legend=False, alphabet=alphabet, letter2legend=letter2legend,
            xlabel=False, ylabel=False)
        plotted_data[label] = used_data
        if ylim is not None:
            axis.set_ylim(ylim)
            axis.set_yticklabels(abs(axis.get_yticks()))
        axis.set_ylabel(label)
        axis.set_xlabel("")
        axis.yaxis.set_label_position("right")
    if xlabel is None:
        xlabel = "Position in read"
    plt.xlabel(xlabel, labelpad=10)
    if ylabel is None:
        ylabel = "Proportion"
    plt.ylabel(ylabel, labelpad=30)
    plt.title("")
    label2handle = dict(islice(zip(labels, handles), 5))
    legend_holder.legend(
        handles=[label2handle[nucl] for nucl in alphabet],
        labels=alphabet,
        loc=7)
    if title is not None:
        fig.suptitle(title)
    if fig_path is not None:
        plt.savefig(fig_path)
    return (fig, axes, data_labels, plotted_data)


def plot_base_scaled_proportions_along_read(
        datas,
        data_labels=None, fig_path=None,
        xlabel=None, ylabel=None,
        alphabet=None, letter2legend=None,
        fig=None, axes=None, flip_bars=False, ylim=None,
        title=None):
    """
    Plot bar graphs representing read composition using a representation
    where bars are stacks where nucleotides are represented according
    to their proportion at a given position in reads.

    *datas* should be an iterable of tables where the rows correspond to the
    nucleotides and the columns correspond to positions along a read. A cell is
    expected to contain the proportion of each nucleotide at the corresponding
    position along the reads, scaled with respect to some background values.

    If provided, *data_labels* will be used to label the individual bar plots,
    and should be an iterable of labels matching *datas*.

    If *fig_path* is set, the figure will be saved using this file path.
    By default, no figure is saved.
    """
    if alphabet is None:
        alphabet = "ACGTN"
    if data_labels is None:
        data_labels = [str(i) for i in range(len(datas))]
    if axes is None:
        (fig, axes, data_labels) = vstack_fig_setup(datas, data_labels)
        legend_holder = fig
    else:
        if fig is None:
            legend_holder = axes[0]
        else:
            legend_holder = fig
    plotted_data = {}
    for (nucl_table, axis, label) in zip(datas, axes, data_labels):
        ((handles, labels), used_data) = plot_scaled_proportions(
            nucl_table, axis=axis, flip_bars=flip_bars,
            legend=False, alphabet=alphabet, letter2legend=letter2legend,
            xlabel=False, ylabel=False)
        plotted_data[label] = used_data
        if ylim is not None:
            axis.set_ylim(ylim)
            axis.set_yticklabels(abs(axis.get_yticks()))
        axis.set_ylabel(label)
        axis.set_xlabel("")
        axis.yaxis.set_label_position("right")
    if xlabel is None:
        xlabel = "Position in read"
    plt.xlabel(xlabel, labelpad=10)
    if ylabel is None:
        ylabel = "Scaled proportion"
    plt.ylabel(ylabel, labelpad=30)
    plt.title("")
    label2handle = dict(islice(zip(labels, handles), 5))
    legend_holder.legend(
        handles=[label2handle[nucl] for nucl in alphabet],
        labels=alphabet,
        loc=7)
    if title is not None:
        fig.suptitle(title)
    if fig_path is not None:
        plt.savefig(fig_path)
    return (fig, axes, data_labels, plotted_data)
