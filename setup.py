# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
#from Cython.Build import cythonize
from distutils.extension import Extension
from pysam import get_include as pysam_get_include

name = "libreads"

# Adapted from Biopython
__version__ = "Undefined"
for line in open("%s/__init__.py" % name):
    if (line.startswith('__version__')):
        exec(line.strip())


# https://github.com/cython/cython/blob/master/docs/src/reference/compilation.rst#configuring-the-c-build
extensions = [
    Extension(
        "libreads.bam2fq", ["libreads/bam2fq.pyx"],
        include_dirs=pysam_get_include()),
    ]

setup(
    name=name,
    version=__version__,
    description="Miscellaneous things to make stats and plots from reads.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=find_packages(),
    # scripts=["scripts/"],
    ext_modules = extensions,
    setup_requires=[
        "wheel",
        "cython",
        #"pysam",
    ],
    install_requires=[
        "pysam",
        #"libworkflows @ git+https://gitlab+deploy-token-31:isEzpsgbNf2sJMdUDy2g@gitlab.pasteur.fr/bli/libworkflows.git",
        "matplotlib",
        #"networkx",
        "numpy",
        "pandas",
        "mappy",
        #"pyBigWig",
        #"pybedtools",
        #"scipy",
        #"seaborn",
    ],
    )
    #ext_modules = cythonize("libsmallrna/libsmallrna.pyx"),
    #install_requires=["cytoolz"],
    #zip_safe=False
